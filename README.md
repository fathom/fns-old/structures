# Structures (aka: learning structures)

This repository is a collection of Structures that are defined and developed by an open community. They are available for anybody to use, as they wish, for their own learning.

_What are Structures?_   

They are any tools, systems, or techniques that people can use to "structure" their learning. Some possible examples could be:  
-   a system for running online study groups
-   a set of smart contracts for crowdfunding lectures
-   an accountability system that keeps you (and any study partners) on track
-   a method for taking notes

...or anything else that can be used for _structure_ in one's learning.

## Motivation

Formal structures are incredibly powerful for learning, but they often get
trapped in an institutional "school" context.  This repository is intended to be a context agnostic home for learning structures that anyone can access, use, or contribute to.

<br>

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Structures (aka: learning structures)](#structures-aka-learning-structures)
    - [Motivation](#motivation)
- [How can you use a Structure?](#how-can-you-use-a-structure)
- [How can you create a new Structure?](#how-can-you-create-a-new-structure)
    - [Step 1. Propose a new Structure](#step-1-propose-a-new-structure)
        - [Using a Structure in an Adventure](#using-a-structure-in-an-adventure)
    - [Step 2. Format, Review and Merge the Structure](#step-2-format-review-and-merge-the-structure)
        - [Format](#format)
        - [Review and Merge](#review-and-merge)
    - [Step 3. Maintaining the Structure](#step-3-maintaining-the-structure)
- [Editors](#editors)

<!-- markdown-toc end -->

<br>

# How can you use a Structure?

You can use a Structure anyway you want to. If it helps your learning, then you're using it well. The best way to start is by looking for one in this repo that interests you, and then trying it out for yourself.

The Structure's name may give you an initial idea of what it's about. Each Structure will also have documentation and examples to help explain how it can be used in learning. But don't feel bound to those instructions: feel free to adapt or extend Structures in order to support _your_ learning. 

You can copy, clone, download or fork any of the Structures here. And if you have any questions about a Structure -- or suggestions for improving it -- you can contact its Maintainers directly. 

<br>

# How can you create a new Structure?

Do you have an idea for a learning structure that you'd like to propose? Anyone is welcome to contribute!   

Just follow the steps below:

<br>

## Step 1. Propose a new Structure

Start by opening a ~Proposal issue. In it you should describe the Structure, how it helps with learning, and how it can be implemented in actual practice. 

- #### The Power of Examples
  Structures come from real people and are rooted in actual learning experiences. The Structure you're proposing should be something that: you've actually used, you're planning to use, or you've observed firsthand.

- #### Using a Structure in an Adventure
  If you need to to generate examples for your Structure, the [Learning Adventure](https://gitlab.com/fathom/fns/structures/tree/master/1-learning-adventures) (aka: Adventure) is a format you may find helpful. A Structure, itself, it was designed to enable people to experiment with other Structures. The Adventure helps you define a self-directed learning plan and then execute on it, while also documenting and reflecting on the process. Thus you can consciously incorporate your Structure within your planned Adventure, and then document how it works in the context of your own learning. This is a good way to build out the first examples of a Structure's use in real life.

The issue you've opened will help you organize the fundamental aspects of the Structure. Once you've got them in place, it's time to move on to the next step: formatting and submitting the Structure for review.

<br>

## Step 2. Format, Review and Merge the Structure

When you're ready to have your Structure reviewed, open a ~Proposal Merge Request (**MR**) in this repository. Along with that, you'll prepare a markdown document, as follows:  

- ### Format
  Create a `README.md` document in markdown to define your Structure (for examples, you can take a look at other Structures published to this repo). 
  The top of the page must include some metadata (YAML frontmatter):  

   ```
   number: 
   title: 
   maintainers:
   ```
   The `number` will be assigned later by one of the [editors](#editors). The `title` is the name of the Structure you're submitting. The `maintainers` should list you, as the Structure's author (plus, optionally, any co-maintainers -- see more info below, in [Step 3](#step-3-maintaining-the-structure)).    

   After that part, the Structure's README should contain the following sections (which correlate with the contents in your ~Proposal issue):  
   -  **Abstract**: a high level definition of the Structure and its goals
   -  **Implementation**: anything information that's essential or even helpful to know in using the Structure
   -  **Examples**: documented examples of the Structure's actual use (note: this can link to other sources)

   Beyond that, feel free to include other sections if you think they're important. For example, an _Assets_ section (e.g., images, documentation, code) could be useful; or a _References_ section could point out sources from which inspiration was drawn.


- ### Review and Merge
  The Merge Request for your Structure will be reviewed by the [editors](#editors) of this repository. They will be checking that the proposal is correctly formatted, sufficiently documented, and logically consistent. 

  During the review phase, the editors will be in communication with you about any additions, clarifications, or revisions that are needed.

  Once the Structure is ready, the editor will assign it a sequential number and merge it in. At this point it's part of Structures and is available for others to use!

<br>

## Step 3. Maintaining the Structure

A structure isn't set in stone when it's merged in. Others can contribute
to it and propose changes. As the author you'll be the *maintainer* of the
structure. This means it'll be your responsibility to respond to contributors, 
answer their questions, and work with them to approve or 
reject additional proposals. 

If you want, you can assign other individuals as co-maintainers. You can even 
transfer your role to someone else and step down if you need to. 
Just keep in mind that it's important for a structure to be well maintained 
so that it can continue to provide relevance and value to its community of learners. 

<br>

# Editors

The editors are the ones responsible for maintaining this repository. They
review new Structure submissions, manage changes in maintainers, and generally make sure everything here runs smoothly. 

The current editors are:

-   Jared Pereira (@jaredpereira)
-   Michiya Hibino (@mhibino)

If you'd like to be an editor, or think there should be some changes, please
open an issue in this repository :).


