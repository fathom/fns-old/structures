<!-- This is for proposing a new Learning Structure (aka: Structure) --> 

<!-- A Structure is any tool, system, or technique that helps people "structure" their learning -->
<!-- (https://gitlab.com/fathom/fns/structures) -->

--------------------------------------------------------------------------------

## Describe the proposed Structure:
<!-- What is it? How does it help to structure one's learning? What problem does it solve for the learner? -->


## Implementation:
<!-- How does the structure actually work, in practice? What steps would you need to follow? --> 
<!-- Include any information people ought to know when trying the structure out -->


## Examples:
<!-- Describe actual examples of use, such as how you've used it in your own learning -->
<!-- This can also include links to other sources (images, documentation, etc.) -->
<!-- See Adventures (https://gitlab.com/fathom/fns/structures/tree/master/1-learning-adventures) -->
<!-- for a format that can help with generating examples of Structures within learning -->


--------------------------------------------------------------------------------

**Note**:    
Post to the discussion below to help consider and develop this proposal.   

And if you try the Structure out for yourself, please contribute your reflections and feedback!


--------------------------------------------------------------------------------
<!-- Please don't delete!  -->
/label ~"Proposal"

