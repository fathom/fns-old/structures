`Proposal` Merge Request (MR)
--------------------------------------------------------------------------------

## What does this MR add?
<!-- Briefly describe what this MR is about -->



## Related Issues
<!-- Mention the issue(s) this MR closes (with "Resolves #") or is related to (with "Relates to #") -->



## Format the Structure
The structure should be defined in a document written in markdown, and should contain the following:   
<!-- For more information: https://gitlab.com/fathom/fns/structures/blob/master/README.md#format -->

- [ ] **Title**: the name of the structure
- [ ] **Maintainers**: one or more individuals who will be maintaining the structure
- [ ] **Abstract**: a high level description of the structure and its goals
- [ ] **Examples**: a list of links to implementations of this structure
- [ ] **Assets**: links to any supplementary materials (e.g., images, documentation, code, etc.) 
that provide helpful evidence, context, and support
- [ ] **References** (optional): a list of references that were used in the structure



--------------------------------------------------------------------------------
<!-- Please don't delete!  -->
/label ~"Proposal"
