---
number: 1
title: Learning Adventures
maintainers: Jared Pereira (jared(at)awarm.space)
---

## Abstract

The Learning Adventure offers a standardized format for people to engage in formal learning outside of an institutional context.

The goal is to capture a learner's intents, processes and reflections in a way that can be documented and shared. This allows others to benefit from past Adventures, and build upon them. Learning begets more learning!

Each Adventure consists of three parts: a Map, a Log, and Artifacts.


## The Process

### 1. The Map

The first step of an Adventure is creating the Map, a document that lays out the learning plan. It should include the following:   
- content: _what_ the learner will be tackling
- questions: seeking these answers will drive the inquiry
- process: _how_ the learner will go about the Adventure
- participants: _who_ will be involved
- period: proposed start and end dates for running the Adventure

The Map should also link to three things that will be key to carrying out the Adventure: the Location, the Log, and Artifacts.

##### The Location
The map should designate an online location where the Adventure is being run. 
This could be a personal website, a community platform like [are.na](https://are.na), a code repository such as [gitlab](https://gitlab.com/), or even a place like a [youtube](https://youtube.com) channel.

### 2. The Log
The Log is the learner's account of their Adventure. Whereas the Map lays out the plan in advance, the Log describes how the Adventure actually unfolds. It could be kept as a journal or blog, even embedded in the Location selected for the Adventure. It needn't be super formal or lengthy, but it should try to capture key moments on the journey. Finally, the Log should end with a reflection on the Adventure: how it went, which practices were and weren't effective, and thoughts on how it could be improved upon. The Log should be linked to from the Map.

### 3. Artifacts
A learning Adventure should produce some Artifacts, or outputs, along the way. They could take any of a number of forms, such as source code, photos, drawings, or video. They might even consist of writings that are incorporated into the Log. As outputs of the learning process, artifacts needn't be pretty or polished. They can be reflective of an interim state. However, it's helpful if they are given some context. They should be assembled as files or links, and published along with the Map and Log. 


## Collecting Adventures

Hosting Adventures in a community repository helps boost their accountability for the adventurer. It also enables greater sharing of learning.

A community repository should enforce certain standards around Adventures (e.g., that they should be well defined, scoped to a particular field, ensure that the learner has serious intent to follow through, etc.). The easiest way to do this is to have assigned "Editors" who vet every learning adventure.

## Goal

The goal in creating the Adventures process is to make experiments in self-directed learning achievable and accessible.

